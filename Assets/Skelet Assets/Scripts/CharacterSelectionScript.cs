﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CharacterSelectionScript : MonoBehaviour {
    public Button P1Character1;
    public Button P1Character2;
    public Button P1Character3;
    public Button P2Character1;
    public Button P2Character2;
    public Button P2Character3;
    public Button Fight;
    public Image P1Image;
    public Image P2Image;
    public Image Character1Image;
    public Image Character2Image;
    public Image Character3Image;
    public Image NoCharacterSelected;
    private string P1Selection;
    private string P2Selection;
    public string Fightscene;
    
    // Use this for initialization
    void Start () {
        P1Selection = "None";
        P2Selection = "None";
        P1Image = NoCharacterSelected;
        P2Image = NoCharacterSelected;
	}
	
    public void FightButton()
    {

        if(P1Selection != "None" && P2Selection != "None")
        {
            PlayerPrefs.SetString("P1Character", P1Selection);
            PlayerPrefs.SetString("P2Character", P2Selection);
            SceneManager.LoadScene(Fightscene);
        }
        else
        {
            //Smid noget error her!
        }
    }

    public void P1Character1Button()
    {
        P1Selection = "Character1";
        P1Image = Character1Image;
    }

    public void P1Character2Button()
    {
        P1Selection = "Character2";
        P1Image = Character2Image;
    }

    public void P1Character3Button()
    {
        P1Selection = "Character3";
        P1Image = Character3Image;
    }

    public void P2Character1Button()
    {
        P2Selection = "Character1";
        P2Image = Character1Image;
    }

    public void P2Character2Button()
    {
        P2Selection = "Character2";
        P2Image = Character2Image;
    }

    public void P2Character3Button()
    {
        P2Selection = "Character3";
        P2Image = Character3Image;
    }

}