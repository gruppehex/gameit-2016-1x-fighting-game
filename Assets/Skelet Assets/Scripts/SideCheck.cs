﻿using System.Collections;
using UnityEngine;

public class SideCheck : MonoBehaviour
{
    public GameObject player;

    private void OnTriggerEnter(Collider coll)
    {
        print("hello");
        if (coll.gameObject.tag == "Ground")
        {
            print("checkwall");
            if (gameObject.name == "sidecheckright")
            {
                print("right");
                Debug.Log("hitright");
                this.player.SendMessage("HitWall", "right");
            }
            else
            {
                print("left");
                Debug.Log("hitleft");
                this.player.SendMessage("HitWall", "left");
            }
        }
    }

    private void OnTriggerExit(Collider coll)
    {
        print("exit");
        if (coll.gameObject.tag == "Ground")
        {
            print("exitwall");
            if (gameObject.name == "sidecheckright")
            {
                print("right");

                this.player.SendMessage("ExitWall", "right");
            }
            else
            {
                print("left");

                this.player.SendMessage("ExitWall", "left");
            }
        }
    }

    private void Start()
    {
        this.player = GameObject.Find("player");
    }
}