﻿namespace LightBrawler.Character
{
    using System.Collections;
    using UnityEngine;

    public class CharacterController : MonoBehaviour
    {
        [SerializeField]
        private float gravity;

        private Collider2D groundCheck;
        private bool hasjumped;

        [SerializeField]
        private float jumpPower;

        [SerializeField]
        private int jumptime;

        [SerializeField]
        private int jumptimemax;

        [SerializeField]
        private float movespeed;

        private bool nearWallRight, nearWallLeft;
        private bool onGround;

        [SerializeField]
        private Rigidbody player3D;

        public void ExitWall(string side)
        {
            Debug.Log("hey");
            if (side == "right")
                nearWallRight = false;
            if (side == "left")
                nearWallLeft = false;
        }

        public void GroundCheck()
        {
            this.onGround = true;
            this.hasjumped = false;
            Debug.Log("changed");
            this.jumptime = 0;
        }

        public void HitWall(string side)
        {
            Debug.Log("hey");
            if (side == "right")
                nearWallRight = true;
            if (side == "left")
                nearWallLeft = true;
        }

        private void FixedUpdate()
        {
            this.Gravity();
        }

        private void Gravity()
        {
            player3D.AddForce(0, gravity, 0);
        }

        private void HorizontalMovement()
        {
            if (Input.GetAxis("Horizontal") > 0 && !nearWallRight)
            {
                //player3D.AddForce(new Vector3(Input.GetAxis("Horizontal") * this.movespeed * Time.deltaTime * 30, 0, 0), ForceMode.VelocityChange);
                //player3D.velocity = new Vector3(Input.GetAxis("Horizontal") * this.movespeed * Time.deltaTime * 1, 0, 0);
                transform.Translate(Input.GetAxis("Horizontal") * this.movespeed * Time.deltaTime * 1, 0, 0);
            }
            else if (Input.GetAxis("Horizontal") < 0 && !nearWallLeft)
            {
                //player3D.AddForce(new Vector3(Input.GetAxis("Horizontal") * this.movespeed * Time.deltaTime * 30, 0, 0), ForceMode.VelocityChange);
                //player3D.velocity = new Vector3(Input.GetAxis("Horizontal") * this.movespeed * Time.deltaTime * 1, 0, 0);
                transform.Translate(Input.GetAxis("Horizontal") * this.movespeed * Time.deltaTime * 1, 0, 0);
            }
            ////this.player3D.AddForce(Input.GetAxis("Horizontal") * this.movespeed * Time.deltaTime * 100, 0, 0);
            //player3D.velocity = new Vector3(Input.GetAxis("Horizontal") * this.movespeed * Time.deltaTime * 100, 0, 0);
        }

        private void Jump()
        {
            if (this.jumptime <= this.jumptimemax)
            {
                player3D.AddForce(0, 10 * this.jumpPower, 0);
                this.hasjumped = true;
                this.onGround = false;
                this.jumptime++;
            }

            Debug.Log(jumptime);
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            Debug.Log("hej");
            if (other.tag == "Ground")
            {
                this.onGround = true;
                this.hasjumped = false;
                Debug.Log("changed");
                this.jumptime = 0;
            }
        }

        ////private void CheckGround()
        ////{
        ////    if (Physics.Raycast(transform.position, Vector3.down, 1))
        ////    {
        ////        this.onGround = true;
        ////        this.hasjumped = false;
        ////    }
        ////}
        private void Start()
        {
            this.player3D = gameObject.GetComponent<Rigidbody>();
            nearWallRight = false;
        }

        private void Update()
        {
            this.HorizontalMovement();
            print(nearWallRight);
            if (Input.GetKey(KeyCode.W))
            {
                this.Jump();
            }
            else if (Input.GetAxis("Vertical") == 1 && !this.hasjumped)
            {
                this.Jump();
            }
            else if (Input.GetKey(KeyCode.Joystick1Button0) && !this.hasjumped)
            {
                this.Jump();
            }

            ////this.CheckGround();
        }
    }
}