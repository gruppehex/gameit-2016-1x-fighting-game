﻿namespace LightBrawler.Character
{
    using System.Collections;
    using UnityEngine;

    public class otherController : MonoBehaviour
    {
        [SerializeField]
        private float gravity, jumpgravity;

        private bool hasjumped;

        [SerializeField]
        private float jumpPower;

        [SerializeField]
        private int jumptime;

        [SerializeField]
        private int jumptimemax;

        [SerializeField]
        private float movespeed;

        private bool onGround;

        [SerializeField]
        private Rigidbody player3D;

        private void FixedUpdate()
        {
            if (!onGround)
                this.Gravity();
        }

        private void Gravity()
        {
            player3D.AddForce(0, gravity, 0);
        }

        private void HorizontalMovement()
        {
            this.player3D.AddForce(Input.GetAxis("Horizontal") * this.movespeed * Time.deltaTime * 100, 0, 0);
            ////player3D.velocity = Vector3 (Input.GetAxis("Horizontal") * Movespeed * Time.deltaTime * 100, 0, 0);
        }

        private void Jump()
        {
            if (!hasjumped)
            {
                if (this.jumptime <= this.jumptimemax)
                {
                    player3D.AddForce(0, 10 * this.jumpPower, 0);
                    this.onGround = false;
                    this.jumptime++;
                }
            }
            Debug.Log(jumptime);
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.tag == "Ground")
            {
                this.onGround = true;
                this.hasjumped = false;
                Debug.Log("changed");
                this.jumptime = 0;
            }
        }

        ////private void CheckGround()
        ////{
        ////    if (Physics.Raycast(transform.position, Vector3.down, 1))
        ////    {
        ////        this.onGround = true;
        ////        this.hasjumped = false;
        ////    }
        ////}
        private void Start()
        {
            this.onGround = false;
            this.player3D = gameObject.GetComponent<Rigidbody>();
        }

        private void Update()
        {
            this.HorizontalMovement();

            if (Input.GetKey(KeyCode.W))
            {
                this.Jump();
            }
            else if (Input.GetAxis("Vertical") == 1)
            {
                this.Jump();
            }
            else if (Input.GetKey(KeyCode.Joystick1Button0))
            {
                this.Jump();
            }

            if (Input.GetKeyUp(KeyCode.W))
            {
                hasjumped = true;
            }
            ////this.CheckGround();
        }
    }
}